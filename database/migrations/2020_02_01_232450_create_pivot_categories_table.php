<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePivotCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pivot_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('article_id')
                    ->foreign('article_id')
                    ->references('id')
                    ->on('articles')
                    ->onDelete('cascade');
            $table->bigInteger('category_id')
                    ->foreign('category_id')
                    ->references('id')
                    ->on('categories')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pivot_categories');
    }
}
