<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profile;


class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "email" => "apirahman55@gmail.com",
            "username" => "apirahman",
            "password" => "rara9999",
            "role" => 1
        ]);
        Profile::create([
            "fullname" => "Api Rahman",
            "user_id" => 1
        ]);
    }
}
