<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    
    protected $fillable = [
        "name"
    ];

    public function article() {
        return $this->belongsToMany(Article::class, 'pivot_categories');
    }

    protected $hidden = [
        "created_at", "updated_at", "pivot"
    ];
}
