<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = "tags";
    
    protected $fillable = [
        "name"
    ];

    protected $hidden = [
        "created_at", "updated_at", "pivot"
    ];

    public function article() {
        return $this->belongsToMany(Article::class, 'pivot_tags');
    }
}
