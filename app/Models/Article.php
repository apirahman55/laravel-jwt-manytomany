<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = "articles";
    
    protected $fillable = [
        "img_header", "title", "description", "user_id", "slug", "read"
    ];
    
    protected $hidden = [
        "user_id"
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function tag() {
        return $this->belongsToMany(Tag::class, 'pivot_tags');
    }
    public function category() {
        return $this->belongsToMany(Category::class, 'pivot_categories');
    }
    public function comment() {
        return $this->hasMany(Comment::class, 'article_id');
    }
}
