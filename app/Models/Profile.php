<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profiles";

    protected $fillable = [
        "image", "fullname", "bio", "user_id"
    ];

    protected $primaryKey = "id";

    public function user() {
        return $this->belongsTo(User::class);
    }
}
