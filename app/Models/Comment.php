<?php

namespace App\Msodels;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = "comments";
    
    protected $fillable = [
        "comment", "user_id", "article_id"
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
    
    public function article() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
