<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pivot_Category extends Model
{
    protected $table = "pivot_categories";
    
    protected $fillable = [
        "article_id", "category_id"
    ];
}
