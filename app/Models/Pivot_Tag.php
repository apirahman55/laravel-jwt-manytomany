<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pivot_Tag extends Model
{
    protected $table = "pivot_tags";
    
    protected $fillable = [
        "article_id", "tag_id"
    ];
}
