<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class JWTVerify extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     ** @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try{
            $user = JWTAuth::parseToken()->authenticate();
        } catch(JWTException $e) {
            return response()->json(['status' => 'Bad Request'], 500);
        } catch(TokenInvalidException $e) {
            return response()->json(['status' => 'Bad Request'], 500);
            // return response()->json(['status' => 'Token is Invalid']);
        } catch(TokenExpiredException $e) {
            // return response()->json(['status' => 'Bad Request'], 500);
            return response()->json(['status' => 'Token is Expired']);
        }
        return $next($request);
    }
}