<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class Rolecheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = auth('api')->getPayload(auth('api')->getToken()->get())->toArray()['sub'];
        $user = json_decode(
            User::where('id', $token)->first()
        );
        
        if(!$user->role === 1) {
            return response()->make([
                "status" => false,
                "message" => "Unauthorized"
            ], 403);
        }
        
        return $next($request);
    }
}
