<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct(User $user) 
    {
        $this->user = $user;
    }
    
    public function getProfileById()
    {
        $token = auth('api')->getPayload(auth('api')->getToken()->get())->toArray()['sub'];
        $user = $this->user->where('id', $token);
        $profile = json_decode($user->with("profile")->withCount('article')->first());
        
        return response()->make([
            "status" => true,
            "message" => "Successfully get users",
            "data" => $profile
        ], 200);
    }
}
