<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use File;
use Image;
use Carbon\Carbon;
use App\Models\Tag;
use App\Models\Article;
use App\Models\Category;
use App\Models\Pivot_Tag;
use App\Models\Pivot_Category;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct(validator $validator, Article $article) 
    {
        $this->validator = $validator;
        $this->article = $article;
    }
    
    public function getArticle()
    {
        return response()->json([
            "status" => true,
            "message" => "success",
            "data" => $this->article->with(['tag', 'category'])->get()
        ], 200);
    }
    
    public function getArticleById() {
        $token = auth('api')->getPayload(auth('api')->getToken()->get())->toArray()['sub'];
        
        return response()->make([
            "status" => "true",
            "message" => "success",
            "data" => $this->article->where('user_id', $token)->with(['tag', 'category'])->get()
        ], 200);
    }
    
    public function getArticleBySlug($slug) 
    {
        $data = $this->article->where('slug', $slug);
        
        if(!$data->count()) {
            
            return response()->json([
                "status" => false,
                "message" => "cannot find article",
                "data" => $this->article->with(['tag', 'category'])->get()
            ], 402);
            
        }
        
        $data->update([
            "read" => json_decode($data->first())->read+1
        ]);
        
        return response()->json([
            "status" => true,
            "message" => "success",
            "data" => $data->first()
        ], 200);
    }
    
    public function getTags(Tag $tag) {
        return response()->json([
            "status" => true,
            "message" => "success",
            "data" => $tag->get()
        ], 200);
    }
    
    public function getCategories(Category $category) {
        return response()->json([
            "status" => true,
            "message" => "success",
            "data" => $category->get()
        ], 200);
    }
    
    public function postArticle(Request $request)
    {
        $token = auth('api')->getPayload(auth('api')->getToken()->get())->toArray()['sub'];
        $validator = $this->validator::make($request->all(), [
            "title" => "required|min:8",
            "description" => "required|min:8",
            "img_header" => "required|image:jpg,png,jpeg",
            "tag" => "required",
            "category" => "required"
        ]);
        
        if($validator->fails()) {
            
            return response()->json([
                "status" => false,
                "message" => $validator->errors()->first(),
            ], 402);
            
        }
        
        $filename = Carbon::now()->timestamp."_".uniqid().".".$request->file('img_header')->getClientOriginalExtension();
        
        Image::make($request->file('img_header')->getRealPath())->save(public_path('images')."/".$filename);
        
        $slug = explode(" ", substr($request->input('title'), 0, 20));
        $article = json_decode(
            $this->article->create([
                "title" => $request->input('title'),
                "img_header" => $filename,
                "description" => $request->input('description'),
                "slug" => strtolower(implode("-", $slug)."-".rand(0, 9999999)),
                "read" => 0,
                "user_id" => $token
            ])
        );
        
        foreach(explode(",", $request->input('category')) as $category) {
            
            Pivot_Category::create([
                "article_id" => $article->id,
                "category_id" => $category
            ]);
            
        }
        
        foreach(explode(",", $request->input('tag')) as $tag) {
            
            Pivot_Tag::create([
                "article_id" => $article->id,
                "tag_id" => $tag
            ]);
            
        }
        
        return response()->json([
            "status" => true,
            "message" => "Article successfully published",
        ], 200);
    }
    
    public function addTag(Request $request, Tag $tag)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required"
        ]);
        
        if($validator->fails()) {
            
            return response()->json([
                "status" => false,
                "message" => $validator->errors()->first(),
            ], 402);
            
        }
        
        $tag->create([
            "name" => $request->input('name')
        ]);
        
        return response()->json([
            "status" => true,
            "message" => "Tag has been created",
        ], 200);
    }
    
    public function addCategory(Request $request, Category $category)
    {
        $validator = $this->validator::make($request->all(), [
            "name" => "required"
        ]);
        
        if($validator->fails()) {
            
            return response()->json([
                "status" => false,
                "message" => $validator->errors()->first(),
            ], 402);
            
        }

        $category->create([
            "name" => $request->input('name')
        ]);
        
        return response()->json([
            "status" => true,
            "message" => "Category has been created",
        ], 200);
    }
}
