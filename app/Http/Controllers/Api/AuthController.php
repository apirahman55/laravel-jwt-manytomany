<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function __construct(Validator $validator, User $user, Profile $proifle) {
        $this->validator = $validator;
        $this->profile = $proifle;
        $this->user = $user;
    }
    
    public function signin(Request $request)
    {
        $token = null;
        $validator = $this->validator->make($request->all(), [
            "username" => "required",
            "password" => "required"
        ]);
        
        if($validator->fails()) {
            return response()->json([
                "status" => false,
                "message" => $validator->errors()->first()
            ], 402);
        }
        
        if ($token = auth('api')->attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
            
            $checkUser = $this->user->where("username", $request->input('username'))->first();
            
            return response()->json([
                "status" => true,
                "message" => "Login Successfully",
                "data" => [
                    "token" => $token,
                    "role" => json_decode($checkUser)->role
                ]
            ], 200);
            
        } else if ($token = auth('api')->attempt(['email' => $request->input('username'), 'password' => $request->input('password')])) {
            
            $checkUser = $this->user->where("email", $request->input('username'))->first();
            return response()->json([
                "status" => true,
                "message" => "Login Successfully",
                "data" => [
                    "token" => $token,
                    "role" => json_decode($checkUser)->role
                ]
            ], 200);
            
        } else {
            
            return response()->json([
                "status" => false,
                "message" => "Username / Password Incorrect",
                "data" => []
            ], 401);
            
        }
        
    }

    public function signup(Request $request)
    {
        $validator = $this->validator->make($request->all(), [
            "email" => "required|email",
            "username" => "required|min:8",
            "password" => "required|min:8",
            "fullname" => "required|min:8",
        ]);
        
        if($validator->fails()) {
            return response()->json([
                "status" => false,
                "message" => $validator->errors()->first()
            ], 402);
        }
        
        $users = json_decode(
            $this->user->create([
                "email" => $request->input('email'),
                "username" => $request->input('username'),
                "password" => $request->input('password'),
                "role" => 2
            ])
        );
        $this->profile->create([
            "fullname" => $request->input('fullname'),
            "user_id" => $users->id
        ]);
        
        return response()->json([
            "status" => true,
            "message" => "Signup Successfully"
        ]);
    }
}
