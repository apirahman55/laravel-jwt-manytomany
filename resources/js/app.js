import Vue from 'vue';
import Vuetify from 'vuetify';

import './owl-carousel';
import './bootstrap';
import 'owl.carousel/dist/assets/owl.carousel.min.css'
import 'owl.carousel/dist/assets/owl.theme.default.min.css'
import App from './App.vue';
import router from './router';

Vue.component('header-component', require('./components/layouts/HeaderComponent.vue').default);
Vue.component('footer-component', require('./components/layouts/FooterComponent.vue').default);

Vue.use(Vuetify);
router.beforeEach((to, from, next) => {
    document.title = to.meta.title +" | My Blog"
    next()
});


new Vue({
    el: '#app',
    router,
    render: h => h(App),
});
