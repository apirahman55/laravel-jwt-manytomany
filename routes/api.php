<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function() {
    Route::post('/signin', 'Api\AuthController@signin');
    Route::post('/signup', 'Api\AuthController@signup');
    Route::get('/articles', 'Api\ArticleController@getArticle');
    Route::get('/article/{slug}', 'Api\ArticleController@getArticleBySlug');
    // Dashboard
    Route::prefix('user')->group(function() {
        Route::get('/profile', 'Api\UserController@getProfileById')->middleware('jwt'); # Get Profile
        Route::get('/articles', 'Api\ArticleController@getArticleById')->middleware('jwt'); # Add Article
        Route::post('/article', 'Api\ArticleController@postArticle')->middleware('jwt'); # Add Article
        Route::get('/article/tag', 'Api\UserController@getTags')->middleware('jwt'); # Get All Tags
        Route::post('/article/tag', 'Api\ArticleController@addTag')->middleware(['jwt', 'RoleCheck']); # Add Some Tag
        Route::get('/article/category', 'Api\UserController@getCategories')->middleware('jwt'); # Get All Categories
        Route::post('/article/category', 'Api\ArticleController@addCategory')->middleware(['jwt', 'RoleCheck']); # Add Some Category
    });
});